# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import SleepEvent, SleepWithEvent

class SleepAction(Action2):
    EVENT = SleepEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "sleep"
    
class SleepWithAction(Action3):
    EVENT = SleepWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "sleep-with"
