# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3


class BreakEvent(Event2):
    NAME = "break"

    def perform(self):
        self.object.move_to(self.actor.container())
        self.inform("break")

class BreakWithEvent(Event3):
    NAME = "break-with"

    def perform(self):
        if not self.object.has_prop("breakable") or not self.object2.has_prop("breaker"):
            self.fail()
            return self.inform("break-with.failed")
        self.inform("break-with")
