# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class FeedEvent(Event2):
    NAME = "feed"

    def perform(self):
        if not self.object.has_prop("feedable"):
            self.fail()
            return self.inform("feed.failed")
        self.inform("feed")

class FeedWithEvent(Event3):
    NAME = "feed-with"

    def perform(self):
        if not self.object.has_prop("feedable") or not self.object2.has_prop("eatable"):
            self.fail()
            return self.inform("feed-with.failed")
        if self.object.is_empty():
            self.add_prop("object-is-empty")
        self.object.move_to(self.object.container())
        self.inform("feed-with")
