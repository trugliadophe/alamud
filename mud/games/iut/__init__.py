# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, TypeAction, InventoryAction,
    LightOnAction, LightOffAction, LightWithAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction, BreakAction,KillWithAction,KillAction,SleepAction,
    FeedAction,FeedWithAction, SleepWithAction, BlendWithAction, FlushAction, FlushWithAction
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |)"

    return (
        (GoAction       , r"(?:aller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|prendre) %s(\S+)" % DETS),
        (LookAction     , r"(?:r|regarder)"),
        (InspectAction  , r"(?:r|regarder|lire|inspecter|observer) %s(\S+)" % DETS),
        (OpenAction     , r"ouvrir %s(\S+)" % DETS),
        (OpenWithAction , r"ouvrir %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (CloseAction    , r"fermer %s(\S+)" % DETS),
        (TypeAction     , r"(?:taper|[eé]crire|saisir) (\S+)$"),
        (InventoryAction, r"(?:inventaire|inv|i)$"),
        (LightOnAction  , r"allumer %s(\S+)" % DETS),
        (LightOffAction , r"[eé]teindre %s(\S+)" % DETS),
        (LightWithAction, r"allumer %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (DropAction     , r"(?:poser|laisser) %s(\S+)" % DETS),
        (DropInAction   , r"(?:poser|laisser|mettre|verser) %s(\S+) (?:dans |sur |)%s(\S+)" % (DETS,DETS)),
        (PushAction     , r"(?:appuyer|pousser|presser)(?: sur|) %s(\S+)" % DETS),
        (TeleportAction , r"tele(?:porter|) (\S+)"),
        (EnterAction    , r"entrer"),
        (LeaveAction    , r"sortir|partir"),
        (BreakAction    , r"lancer|jeter|casser %s(\S+)" % DETS),
        (KillAction     , r"tuer %s(\S+)" % DETS),
        (KillWithAction , r"tuer %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (FeedAction  	, r"nourrir %s(\S+)" % DETS),
        (FeedWithAction	, r"nourrir %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (FlushAction    , r"vider %s(\S+)" % DETS),
        (FlushWithAction, r"vider %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (SleepAction    , r"endormir %s(\S+)" % DETS),
        (SleepWithAction, r"endormir %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (BlendWithAction, r"m[eé]langer %s(\S+) avec %s(\w+)" % (DETS,DETS))
    )
